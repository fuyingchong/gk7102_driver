
#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include "7102_led.h"
#include <sys/ioctl.h>
#include <stdlib.h>
#include <string.h>




#define DEBUG
#ifdef DEBUG
#define LOG_INFO printf
#else
#define LOG_INFO 
#endif /* debug */


int itkled_fd = -1;
//驱动对应的设备节点，目前要手动创建
//mknod /dev/itkled c 251 0  c-->类型，251主设备号
#define ITKFILE		"/dev/itkled-77"
char *user_buf = NULL;
char itkrd_buf[100] = {0};

int main(int argc, char *argv[])
{
    #if 1
    if(argc != 2)
    {
        printf("please input as ./app led_on\n");
        return -1;
    }
    #endif
    
    itkled_fd = open(ITKFILE,O_RDWR);
	if(itkled_fd < 0)
	{
		printf("open err \n");
		return -1;
	}
	LOG_INFO("open ok \n");

#if 0 /*rewd test*/
    //user_buf = (char *)malloc(sizeof(char)); //母鸡为啥子不需要申请内存
    user_buf = "hello world ithink";
    write(itkled_fd, user_buf,20);

    memset(itkrd_buf, 0, sizeof(itkrd_buf));
    read(itkled_fd,itkrd_buf,40);
    printf("read str = %s\n",itkrd_buf);
#endif /*rewd test*/
    
#if 1 /*ioctl test*/
    if(strcmp(argv[1],"led_on") == 0)
    {
        ioctl(itkled_fd, itkled_on);
    }
    else if(strcmp(argv[1], "led_off") == 0)
    {
        ioctl(itkled_fd, itkled_off);
    }
    else if(strcmp(argv[1], "itkread_led") == 0)
    {
        ioctl(itkled_fd, itkread_led);
    }
    else
    {
        printf("Unsupported operations");
    }
    #endif /*ioctl test*/
    
	sleep(1);
	close(itkled_fd);
    //free(user_buf);
	LOG_INFO("close ok\n");

	return 0;
}

