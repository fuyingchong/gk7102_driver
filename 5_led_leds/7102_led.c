
#include <linux/module.h>
#include <linux/init.h>
#include <linux/leds.h>
#include <mach/gpio.h>


struct led_classdev itkled_cdev;
#define ITKLED_NAME     "itkled"

extern void gk_gpio_set_out(u32 pin, u32 value);
extern int led_classdev_register(struct device *parent, struct led_classdev *led_cdev);

//void		(*brightness_set)(struct led_classdev *led_cdev, enum led_brightness brightness);
static void itkled_set(struct led_classdev *led_cdev, 
                        enum led_brightness brightness)
{
    printk(KERN_INFO "itkled set\n");

    if(brightness)
    {
        gk_gpio_set_out(15, 1);
    }
    else
    {
        gk_gpio_set_out(15, 0);
    }
}

static int __init gk7102_leds_init(void)
{
    int ret = 0;

    ret = gpio_request(15, "itk_led");
    if(ret != 0)
    {
        printk(KERN_ERR "gpio_request err\n");
    }
        
    itkled_cdev.name            = ITKLED_NAME;
    itkled_cdev.brightness      = 255;
    itkled_cdev.brightness_set  = itkled_set;
    ret = led_classdev_register(NULL, &itkled_cdev);
    if(ret < 0)
    {
        printk(KERN_ERR "led_classdev_register err\n");
        return -1;
    }
    printk(KERN_INFO "classdev register ok\n");

    return 0;
}

static void __init gk7102_leds_exit(void)
{
    gpio_free(15);
    led_classdev_unregister(&itkled_cdev);
    printk(KERN_INFO "led_classdev_unregister\n");
}

module_init(gk7102_leds_init);
module_exit(gk7102_leds_exit);

MODULE_LICENSE("GPL");

