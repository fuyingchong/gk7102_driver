
//模块初始化头文件
#include <linux/module.h>
//文件系统头文件　  filesystem
#include <linux/fs.h>
#include <mach/gpio.h>
//字符设备头文件  character device
#include <linux/cdev.h>
#include "7102_led.h"
#include <asm/uaccess.h>
#include <linux/leds.h>



#define LED_NAME  "itkled_15"
char itk_buf[100] = {0};
static struct led_classdev itkled_dev = {0};


extern void gk_gpio_set_out(u32 pin, u32 value);
//extern int led_classdev_register(struct device *parent, struct led_classdev *led_cdev);


//void (*brightness_set)(struct led_classdev *led_cdev,enum led_brightness brightness);
void itk_ledbri_set(struct led_classdev *led_cdev,enum led_brightness brightness)
{
    printk("itk led brightness set \n");
    
}

//申请
static int __init gk7102_led_init(void)
{
    int ret = 0;
    
    itkled_dev.name             = LED_NAME;
    itkled_dev.brightness       = 255;//这边值范围，暂时随意
    itkled_dev.brightness_set   = itk_ledbri_set;
        
    ret = led_classdev_register(NULL, &itkled_dev);  //未定义警告，不懂为啥子
    if(ret < 0)
    {
        printk("led classdev register err\n");
        return -1;
    }
	//申请成功后亮灯
	//gk_gpio_set_out(15, 1);

    return 0;
}

static void __exit gk7102_led_exit(void)
{
    led_classdev_unregister(&itkled_dev);

	//释放后灭灯
	//gk_gpio_set_out(15,0);
}

module_init(gk7102_led_init);
module_exit(gk7102_led_exit);


MODULE_LICENSE("GPL");


