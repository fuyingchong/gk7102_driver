#ifndef _7102_LED_H
#define _7102_LED_H
//ioctl 标准头
#include <linux/ioctl.h>

//ioctl cmd
#define  MAGIC   'X'

#define itkled_on       _IO(MAGIC, 0x01)
#define itkled_off      _IO(MAGIC, 0x02)
#define itkread_led     _IO(MAGIC, 0x03)


#endif

