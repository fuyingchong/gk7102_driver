#ifndef _7102_LED_H
#define _7102_LED_H
//ioctl 标准头
#include <linux/ioctl.h>

//ioctl cmd

#define GK_ADC_IOC_MAGIC        'A'
#define IOC_ITKADC_SET_CHANNEL     _IOW(GK_ADC_IOC_MAGIC, 0, unsigned int)
#define IOC_ITKADC_GET_CHANNEL     _IOR(GK_ADC_IOC_MAGIC, 1, unsigned int)
#define IOC_ITKADC_GET_DATA        _IOR(GK_ADC_IOC_MAGIC, 2, unsigned int)



#endif

