
//模块初始化头文件
#include <linux/module.h>
//文件系统头文件　  filesystem
#include <linux/fs.h>
#include <linux/miscdevice.h>        //杂设备头文件
#include "7102_adc.h"

#include "mach/gk710x.h"
#include <mach/hardware.h>
#include <mach/io.h>

#define ITKADC_NAME     "itkadc"

#define ADC_CHANNEL_ONE     (0)

#define ADC_VA_BASE             (GK_VA_ADC2)
#define ADC_REG(x)              (ADC_VA_BASE + (x))

#define ADC_ANALOG_VA_BASE      (GK_VA_ADC)
#define ADC_ANALOG_REG(x)       (ADC_ANALOG_VA_BASE + (x))


#define REG_ADC_AUX_ATOP        ADC_ANALOG_REG(0x00) /* read/write */
#define REG_ADC_GPIO            ADC_ANALOG_REG(0x04) /* read/write */
#define REG_ADC_CONTROL         ADC_REG(0x000) /* read/write */
#define REG_ADC_READDATA        ADC_REG(0x004) /* read */
#define REG_ADC_ENABLE          ADC_REG(0x018) /* read/write */
#define REG_ADC_INTCONTROL      ADC_REG(0x044) /* read/write */




//int (*open) (struct inode *, struct file *);
static int itkadc_open(struct inode *inode, struct file *file)
{
    printk(KERN_INFO "itkadc open \n");

    return 0;
}

//int (*release) (struct inode *, struct file *);
static int itkadc_close(struct inode *inode, struct file *file)
{
    printk(KERN_INFO "itkadc close \n");

    return 0;
}

static uint32_t itkadc_getdata(uint8_t index)
{
    uint32_t value;
    value = gk_adc_readl(REG_ADC_READDATA + (index * 0x4));
    return value;
}

//long (*unlocked_ioctl) (struct file *, unsigned int, unsigned long);
static long itkadc_ioctl(struct file *file,unsigned int cmd, unsigned long arg)
{
    uint32_t adc_data = 0;
    
    switch(cmd)
    {
        case IOC_ITKADC_SET_CHANNEL:
            break;

        case IOC_ITKADC_GET_CHANNEL:
            break;

        case IOC_ITKADC_GET_DATA:
            adc_data = itkadc_getdata((u32)ADC_CHANNEL_ONE);
            printk(KERN_INFO "get adc_value = %d\n",adc_data);
            break;

        default:
            break;
    }
    return 0;
}

static struct file_operations itkadc_fops = {
    .owner          = THIS_MODULE,
    .open           = itkadc_open,
    .release        = itkadc_close,
    .unlocked_ioctl = itkadc_ioctl,
};
static struct miscdevice itkadc = {
    .minor      = 133, /***次设备号***/
	.name       = ITKADC_NAME,
	.fops       = &itkadc_fops,
};


static int __init itk_adc_init(void)
{
    int ret = 0;

    ret = misc_register(&itkadc);
    if(ret != 0)
    {
        printk(KERN_ERR "misc_register err\n");
        return -1;
    }

    return ret;
}

static void __exit itk_adc_exit(void)
{
    misc_deregister(&itkadc);
    printk(KERN_INFO "itk_adc_exit succeed\n");
}

module_init(itk_adc_init);
module_exit(itk_adc_exit);

MODULE_LICENSE("GPL");


